#include <stdio.h>
#include "fpioa.h"
#include <cstring>
#include <sstream>
#include "uart.h"
#include "pwm.h"
#include "timer.h"
#include "gpiohs.h"
#include "sysctl.h"
#include "plic.h"

#define UART_NUM    UART_DEVICE_3

#define LED_BLUE    12
#define LED_GREEN   13
#define LED_RED     14

const pwm_device_number_t PWM_DEVICE         = PWM_DEVICE_1;
const pwm_channel_number_t PWM_CHANNEL_BLUE  = PWM_CHANNEL_0;
const pwm_channel_number_t PWM_CHANNEL_GREEN = PWM_CHANNEL_1;
const pwm_channel_number_t PWM_CHANNEL_RED   = PWM_CHANNEL_2;

const timer_device_number_t TIMER_DEVICE   = TIMER_DEVICE_0;
const timer_channel_number_t TIMER_CHANNEL = TIMER_CHANNEL_0;

int printThing(void *_thing)
{
    char buf[20];
    memset(buf, '0', sizeof(buf));
    snprintf(buf, sizeof(buf), "INTERRUPT TEST 1\n");
    buf[sizeof(buf) - 1] = '\0';
    uart_send_data(UART_NUM, buf, strlen(buf));

    return 0;
}

void printThing2()
{
    char buf[20];
    memset(buf, '0', sizeof(buf));
    snprintf(buf, sizeof(buf), "INTERRUPT TEST 2\n");
    buf[sizeof(buf) - 1] = '\0';
    uart_send_data(UART_NUM, buf, strlen(buf));
}

void interruptTest()
{
    // read pin 2 (set by FUNC_GPIOHS2 in fpioa_set_function call)
    if (gpiohs_get_pin(2))
    {
        gpiohs_set_pin(3, GPIO_PV_HIGH);
    }
    else
    {
        gpiohs_set_pin(3, GPIO_PV_LOW);
    }

    char buf[20];
    memset(buf, '0', sizeof(buf));
    snprintf(buf, sizeof(buf), "INTERRUPT TOGGLE\n");
    buf[sizeof(buf) - 1] = '\0';
    uart_send_data(UART_NUM, buf, strlen(buf));
}

void io_mux_init(void)
{
    fpioa_set_function(4, static_cast<fpioa_function_t>(FUNC_UART1_RX + UART_NUM * 2));
    fpioa_set_function(5, static_cast<fpioa_function_t>(FUNC_UART1_TX + UART_NUM * 2));

    // I think this is setting these LEDS to be able to be PWM'd, since PWM and timers
    // seem to be intrinsically linked somehow. I also think the values are incremented by 1
    fpioa_set_function(LED_BLUE,  FUNC_TIMER1_TOGGLE1);
    fpioa_set_function(LED_GREEN, FUNC_TIMER1_TOGGLE2);
    fpioa_set_function(LED_RED,   FUNC_TIMER1_TOGGLE3);
}

void init_gpio()
{
    pwm_init(PWM_DEVICE);
    pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_BLUE,   20000, 0.00);
    pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_GREEN,  20000, 0.50);
    pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_RED,    20000, 1.00);

    pwm_set_enable(PWM_DEVICE, PWM_CHANNEL_BLUE,  1);
    pwm_set_enable(PWM_DEVICE, PWM_CHANNEL_GREEN, 1);
    pwm_set_enable(PWM_DEVICE, PWM_CHANNEL_RED,   1);
}

int adjust_pwm(void*)
{
    static double dutyBlue =  .01;
    static double dutyGreen = .50;
    static double dutyRed =   .99;

    static double incBlue =  .01;
    static double incGreen = .01;
    static double incRed =   .01;

    if (dutyBlue > .99 || dutyBlue < .01)
    {
        incBlue *= -1;
    }
    if (dutyGreen > .99 || dutyGreen < .01)
    {
        incGreen *= -1;
    }
    if (dutyRed > .99 || dutyRed < .01)
    {
        incRed *= -1;
    }

    dutyBlue = dutyBlue + incBlue;
    dutyGreen = dutyGreen + incGreen;
    dutyRed = dutyRed + incRed;

    char buf[320];
    memset(buf, '0', sizeof(buf));
    snprintf(buf, sizeof(buf), "=======\nR: %g\nG: %g\nB: %g\n", dutyRed, dutyGreen, dutyBlue);
    buf[sizeof(buf) - 1] = '\0';
    //uart_send_data(UART_NUM, buf, strlen(buf));

    pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_BLUE,  20000, dutyBlue);
    pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_GREEN, 20000, dutyGreen);
    pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_RED,   20000, dutyRed);

    return 0;
}


int main()
{
    io_mux_init();
    sysctl_enable_irq();
    init_gpio();

    // IRQ init - not working, I may be misunderstanding what this is for
    /*plic_init();
      plic_set_priority(IRQN_GPIOHS0_INTERRUPT, 1);
      plic_irq_register(IRQN_GPIOHS0_INTERRUPT, printThing, NULL);
      plic_irq_enable(IRQN_GPIOHS0_INTERRUPT);*/


    // GPIO init
    // Enable values read on GPIO pin 3 to turn on LED on GPIO pin 2
    // Note: calls to gpiohs_get_pin should use the 3 from argument 2 here, which is weird and confusing
    // see https://github.com/kendryte/kendryte-standalone-demo/blob/develop/gpiohs_led/main.c for
    // how the weird mapping scheme works (PIN_* vs GPIO_*)
    fpioa_set_function(3, FUNC_GPIOHS3);
    gpiohs_set_drive_mode(3, GPIO_DM_OUTPUT);
    gpiohs_set_pin(3, GPIO_PV_HIGH);

    fpioa_set_function(2, FUNC_GPIOHS2);
    gpiohs_set_drive_mode(2, GPIO_DM_INPUT_PULL_UP);
    gpiohs_set_pin_edge(2, GPIO_PE_BOTH);

    // printThing3 will be called on either edge of GPIO pin 2
    gpiohs_set_irq(2, 1, interruptTest);

    uart_init(UART_NUM);
    uart_configure(UART_NUM, 115200, UART_BITWIDTH_8BIT, UART_STOP_1, UART_PARITY_NONE);

    timer_init(TIMER_DEVICE);
    timer_set_interval(TIMER_DEVICE, TIMER_CHANNEL, 1e7);
    timer_irq_register(TIMER_DEVICE, TIMER_CHANNEL, 0, 1, adjust_pwm, NULL);
    timer_set_enable(TIMER_DEVICE, TIMER_CHANNEL, 1);


    while(true) {}
    return 1;
}
