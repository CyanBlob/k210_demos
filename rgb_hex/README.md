Reads 6-digit hex numbers from serial and sets RGB LED to that color
Red pin: 12
Green pin: 13
Blue pin: 14

Note: these are the pins for the built-int RBG LED on the MAix BiT breakout
