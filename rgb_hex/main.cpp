/* Copyright 2018 Canaan Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdio.h>
#include "fpioa.h"
#include <string>
#include <cstring>
#include <sstream>
#include "uart.h"
#include "pwm.h"
#include "timer.h"
#include "dmac.h"
#include "gpiohs.h"
#include "sysctl.h"

#define RECV_LENGTH  2

#define UART_NUM    UART_DEVICE_3

#define LED_BLUE    12
#define LED_GREEN   13
#define LED_RED     14

const pwm_device_number_t PWM_DEVICE =         PWM_DEVICE_1;
const pwm_channel_number_t PWM_CHANNEL_BLUE =  PWM_CHANNEL_0;
const pwm_channel_number_t PWM_CHANNEL_GREEN = PWM_CHANNEL_1;
const pwm_channel_number_t PWM_CHANNEL_RED =   PWM_CHANNEL_2;

const timer_device_number_t TIMER_DEVICE = TIMER_DEVICE_0;
const timer_channel_number_t TIMER_CHANNEL = TIMER_CHANNEL_0;

void io_mux_init(void)
{

  fpioa_set_function(4, static_cast<fpioa_function_t>(FUNC_UART1_RX + UART_NUM * 2));
  fpioa_set_function(5, static_cast<fpioa_function_t>(FUNC_UART1_TX + UART_NUM * 2));

  fpioa_set_function(LED_BLUE,  FUNC_TIMER1_TOGGLE1);
  fpioa_set_function(LED_GREEN, FUNC_TIMER1_TOGGLE2);
  fpioa_set_function(LED_RED,   FUNC_TIMER1_TOGGLE3);
}

void init_gpio()
{
  pwm_init(PWM_DEVICE);
  pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_BLUE,   20000, 0.00);
  pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_GREEN,  20000, 0.50);
  pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_RED,    20000, 1.00);

  pwm_set_enable(PWM_DEVICE, PWM_CHANNEL_BLUE,  1);
  pwm_set_enable(PWM_DEVICE, PWM_CHANNEL_GREEN, 1);
  pwm_set_enable(PWM_DEVICE, PWM_CHANNEL_RED,   1);
}

int main()
{
  io_mux_init();
  plic_init();
  sysctl_enable_irq();

  uart_init(UART_NUM);
  uart_configure(UART_NUM, 115200, UART_BITWIDTH_8BIT, UART_STOP_1, UART_PARITY_NONE);

  init_gpio();

  std::string hello = "Hello World!\n";
  uart_send_data(UART_NUM, hello.c_str(), hello.length());

  uint8_t red[RECV_LENGTH];
  uint8_t blue[RECV_LENGTH];
  uint8_t green[RECV_LENGTH];

  while(true)
  {
    uart_receive_data_dma(UART_NUM, DMAC_CHANNEL0, red,   RECV_LENGTH);
    uart_receive_data_dma(UART_NUM, DMAC_CHANNEL0, green, RECV_LENGTH);
    uart_receive_data_dma(UART_NUM, DMAC_CHANNEL0, blue,  RECV_LENGTH);

    uart_send_data(UART_NUM, (char*)red,   RECV_LENGTH);
    uart_send_data(UART_NUM, (char*)green, RECV_LENGTH);
    uart_send_data(UART_NUM, (char*)blue,  RECV_LENGTH);
    uart_send_data(UART_NUM, "\n", 1);

    double hex = strtol((char*)red, NULL, 16);
    pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_RED,  20000, (100.0 - (hex / 2.55)) / 100.0);

    hex = strtol((char*)green, NULL, 16);
    pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_GREEN, 20000, (100.0 - (hex / 2.55)) / 100.0);

    hex = strtol((char*)blue, NULL, 16);
    pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_BLUE,   20000, (100.0 - (hex / 2.55)) / 100.0);
  }
}
