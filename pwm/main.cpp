/* Copyright 2018 Canaan Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdio.h>
#include "fpioa.h"
#include <string>
#include <cstring>
#include <sstream>
#include "uart.h"
#include "pwm.h"
#include "timer.h"
#include "gpiohs.h"
#include "sysctl.h"

#define RECV_LENTH  4

#define UART_NUM    UART_DEVICE_3

#define LED_BLUE    12
#define LED_GREEN   13
#define LED_RED     14

const pwm_device_number_t PWM_DEVICE =         PWM_DEVICE_1;
const pwm_channel_number_t PWM_CHANNEL_BLUE =  PWM_CHANNEL_0;
const pwm_channel_number_t PWM_CHANNEL_GREEN = PWM_CHANNEL_1;
const pwm_channel_number_t PWM_CHANNEL_RED =   PWM_CHANNEL_2;

const timer_device_number_t TIMER_DEVICE = TIMER_DEVICE_0;
const timer_channel_number_t TIMER_CHANNEL = TIMER_CHANNEL_0;

void io_mux_init(void)
{

  fpioa_set_function(4, static_cast<fpioa_function_t>(FUNC_UART1_RX + UART_NUM * 2));
  fpioa_set_function(5, static_cast<fpioa_function_t>(FUNC_UART1_TX + UART_NUM * 2));

  fpioa_set_function(LED_BLUE,  FUNC_TIMER1_TOGGLE1);
  fpioa_set_function(LED_GREEN, FUNC_TIMER1_TOGGLE2);
  fpioa_set_function(LED_RED,   FUNC_TIMER1_TOGGLE3);
}

void init_gpio()
{
  pwm_init(PWM_DEVICE);
  pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_BLUE,   20000, 0.00);
  pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_GREEN,  20000, 0.50);
  pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_RED,    20000, 1.00);

  pwm_set_enable(PWM_DEVICE, PWM_CHANNEL_BLUE,  1);
  pwm_set_enable(PWM_DEVICE, PWM_CHANNEL_GREEN, 1);
  pwm_set_enable(PWM_DEVICE, PWM_CHANNEL_RED,   1);
}

int adjust_pwm(void*)
{
  static double dutyBlue =  .01;
  static double dutyGreen = .50;
  static double dutyRed =   .99;

  static double incBlue =  .01;
  static double incGreen = .01;
  static double incRed =   .01;

  if (dutyBlue > .99 || dutyBlue < .01)
    incBlue *= -1;
  if (dutyGreen > .99 || dutyGreen < .01)
    incGreen *= -1;
  if (dutyRed > .99 || dutyRed < .01)
    incRed *= -1;

  dutyBlue = dutyBlue + incBlue;
  dutyGreen = dutyGreen + incGreen;
  dutyRed = dutyRed + incRed;

  double freq = pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_BLUE,  20000, dutyBlue);
  pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_GREEN, 20000, dutyGreen);
  pwm_set_frequency(PWM_DEVICE, PWM_CHANNEL_RED,   20000, dutyRed);

  return 0;
}

int main()
{
  io_mux_init();
  plic_init();
  sysctl_enable_irq();

  uart_init(UART_NUM);
  uart_configure(UART_NUM, 115200, UART_BITWIDTH_8BIT, UART_STOP_1, UART_PARITY_NONE);

  init_gpio();

  std::string hello = "Hello World!\n";
  uart_send_data(UART_NUM, hello.c_str(), hello.length());

  timer_init(TIMER_DEVICE);
  timer_set_interval(TIMER_DEVICE, TIMER_CHANNEL, 1e7);
  timer_irq_register(TIMER_DEVICE, TIMER_CHANNEL, 0, 1, adjust_pwm, NULL);
  timer_set_enable(TIMER_DEVICE, TIMER_CHANNEL, 1);

  while(true);
}
