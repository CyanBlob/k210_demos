Sets a "breathing" animation on an RGB LED with pins:
Red: 12
Green: 13
Blue: 14

Note: These are the pins for the MAix BiT built-in RGD LED
